<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_Pelanggaran extends CI_Controller {
	
	public function index()
	{
		$data['title'] = 'Data Pelanggaran';
		$data['content'] = 'data_pelanggaran/index';
		$data['javascript'] = 'data_pelanggaran.js';
		$this->load->view('layout/index', $data);
	}

	public function data()
	{
		$data['title'] = 'Data Pelanggaran';
		$data['content'] = 'data_pelanggaran/data';
		$data['javascript'] = 'data_pelanggaran.js';
		$this->load->view('layout/index', $data);
	}
}
