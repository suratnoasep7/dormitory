<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_Reward extends CI_Controller {
	
	public function index()
	{
		$data['title'] = 'Data Reward';
		$data['content'] = 'data_reward/index';
		$data['javascript'] = 'data_reward.js';
		$this->load->view('layout/index', $data);
	}

	public function data()
	{
		$data['title'] = 'Data Reward';
		$data['content'] = 'data_reward/data';
		$data['javascript'] = 'data_reward.js';
		$this->load->view('layout/index', $data);
	}
}
