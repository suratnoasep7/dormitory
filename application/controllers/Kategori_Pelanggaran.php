<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_Pelanggaran extends CI_Controller {
	
	public function index()
	{
		$data['title'] = 'Master Kategori Pelanggaran';
		$data['content'] = 'kategori_pelanggaran/index';
		$data['javascript'] = 'kategori_pelanggaran.js';
		$this->load->view('layout/index', $data);
	}
}
