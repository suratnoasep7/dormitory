<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_Reward extends CI_Controller {
	
	public function index()
	{
		$data['title'] = 'Master Kategori Reward';
		$data['content'] = 'kategori_reward/index';
		$data['javascript'] = 'kategori_reward.js';
		$this->load->view('layout/index', $data);
	}
}
