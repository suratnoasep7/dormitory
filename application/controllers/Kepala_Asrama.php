<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kepala_Asrama extends CI_Controller {
	
	public function index()
	{
		$data['title'] = 'Kepala Asrama';
		$data['content'] = 'kepala_asrama/index';
		$data['javascript'] = 'kepala_asrama.js';
		$this->load->view('layout/index', $data);
	}
}
