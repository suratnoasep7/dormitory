<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kepala_Asrama_Siswa extends CI_Controller {
	
	public function index()
	{
		$data['title'] = 'Kepala Asrama Siswa';
		$data['content'] = 'kepala_asrama_siswa/index';
		$data['javascript'] = 'kepala_asrama_siswa.js';
		$this->load->view('layout/index', $data);
	}
}
