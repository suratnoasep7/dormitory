<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggaran_Kepala extends CI_Controller {
	
	public function index()
	{
		$data['title'] = 'Data Pelanggaran';
		$data['content'] = 'pelanggaran_kepala/index';
		$data['javascript'] = 'pelanggaran_kepala.js';
		$this->load->view('layout/index', $data);
	}

	public function data()
	{
		$data['title'] = 'Data Pelanggaran';
		$data['content'] = 'pelanggaran_kepala/data';
		$data['javascript'] = 'pelanggaran_kepala.js';
		$this->load->view('layout/index', $data);
	}
}
