<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';


class Data_Pelanggaran extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Pelanggaran_Model']);
    }

    public function index_get() 
    {
        $headers = $this->input->request_headers();


        if(!array_key_exists('Authorization', $headers)) {
            return $this->messageUnAthorized();
        }

        if(empty($headers['Authorization'])) {
            return $this->messageUnAthorized();
        }

        $decodedToken = AUTHORIZATION::checkToken($headers['Authorization'])->num_rows();

        if($decodedToken == 0) {
            return $this->messageError();
        }

        $dataPelanggaran = null;
        if($this->session->userdata('type') == 'mahasiswa') {
            $dataPelanggaran = $this->Pelanggaran_Model->pelanggaranMahasiswa()->result_array();
        } else {
            $dataPelanggaran = $this->Pelanggaran_Model->pelanggaranSiswa()->result_array();
        }

        
        $response = array('status' => true, 'data' => $dataPelanggaran);
        return $this->set_response($response, REST_Controller::HTTP_OK);
    }

    private function messageUnAthorized() 
    {
        $data = array();
        $data['message'] = 'Data Tidak Ditemukan';
        $data['data'] = [];
        $data['status'] = false;
        $this->set_response($data, REST_Controller::HTTP_UNAUTHORIZED);
    }

    private function messageError() 
    {
        $data = array();
        $data['message'] = 'ISI Data Dengan Baik Dan Benar';
        $data['data'] = [];
        $data['status'] = false;
        $this->set_response($data, REST_Controller::HTTP_BAD_REQUEST);
    }

    private function messageSuccess() 
    {
        $data = array();
        $data['message'] = 'SUCCESS';
        $data['status'] = true;
        $this->set_response($data, REST_Controller::HTTP_OK);
    }

    private function messageDataPelanggaran($pelanggaran) 
    {
        $data = array();
        $data['message'] = 'SUCCESS';
        $data['data'] = $pelanggaran;
        $data['status'] = true;
        $this->set_response($data, REST_Controller::HTTP_OK);
    }

    
}