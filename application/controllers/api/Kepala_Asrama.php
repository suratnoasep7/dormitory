<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';


class Kepala_Asrama extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Kepala_Asrama_Model']);
    }

    public function index_get() 
    {
        $headers = $this->input->request_headers();


        if(!array_key_exists('Authorization', $headers)) {
            return $this->messageUnAthorized();
        }

        if(empty($headers['Authorization'])) {
            return $this->messageUnAthorized();
        }

        $decodedToken = AUTHORIZATION::checkToken($headers['Authorization'])->num_rows();

        if($decodedToken == 0) {
            return $this->messageError();
        }

        $dataKepalaAsrama = $this->Kepala_Asrama_Model->data_asrama($this->session->userdata('id_pegawai'))->result_array();
        $response = array('status' => true, 'data' => $dataKepalaAsrama);
        return $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function index_post() 
    {

        $this->form_validation->set_rules('kode_kategori_asrama','NIM / NISN','required');
        $this->form_validation->set_rules('nama_kategori_asrama','NAMA','required');

        $headers = $this->input->request_headers();

        if(empty($headers['Authorization'])) {
            $this->messageUnAthorized();
        }

        $decodedToken = AUTHORIZATION::checkToken($headers['Authorization'])->num_rows();

        if($decodedToken == 0) {
            $this->messageError();
        }

        if($this->form_validation->run() == false) {
            $this->messageError();
        }

        if($this->insertDataKepalaAsrama()) {
            $this->messageSuccess();
        } else {
            $this->messageError();
        }
    }


    private function messageUnAthorized() 
    {
        $data = array();
        $data['message'] = 'Data Tidak Ditemukan';
        $data['data'] = [];
        $data['status'] = false;
        $this->set_response($data, REST_Controller::HTTP_UNAUTHORIZED);
    }

    private function messageError() 
    {
        $data = array();
        $data['message'] = 'ISI Data Dengan Baik Dan Benar';
        $data['data'] = [];
        $data['status'] = false;
        $this->set_response($data, REST_Controller::HTTP_BAD_REQUEST);
    }

    private function messageSuccess() 
    {
        $data = array();
        $data['message'] = 'SUCCESS';
        $data['status'] = true;
        $this->set_response($data, REST_Controller::HTTP_OK);
    }

    private function insertDataKepalaAsrama() 
    {
        if($this->input->post('tipe') == "0") {
            $data = [
                'id_pelanggaran' => $this->input->post('pelanggaran'),
                'tindakan_pelanggaran' => $this->input->post('tindakan_pelanggaran'),
                'respon_pelanggaran' => $this->input->post('respon_pelanggaran'),
                'keterangan_pelanggaran' => $this->input->post('keterangan'),
                'tanggal_pelanggaran' => $this->input->post('tanggal_pelanggaran'),
                'nomor' => $this->input->post('nomor'),
                'type' => $this->session->userdata('type'),
                'status' => 1,
                'created_by' => 1
            ];
            $response = $this->Kepala_Asrama_Model->save($data);
            return $response;
        } else {
            $data = [
                'id_reward' => $this->input->post('reward'),
                'tindakan_reward' => $this->input->post('tindakan_reward'),
                'respon_reward' => $this->input->post('respon_reward'),
                'keterangan_reward' => $this->input->post('keterangan'),
                'nomor' => $this->input->post('nomor'),
                'tanggal_reward' => $this->input->post('tanggal_reward'),
                'type' => $this->session->userdata('type'),
                'status' => 1,
                'created_by' => 1
            ];
            $response = $this->Kepala_Asrama_Model->saveReward($data);
            return $response;
        }
    }

}