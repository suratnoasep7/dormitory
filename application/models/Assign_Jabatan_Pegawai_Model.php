<?php

class Assign_Jabatan_Pegawai_Model extends CI_Model {
    
    protected $table = 'assign_jabatan_pegawai';

    protected $primaryKey = 'id';

    public function assign_jabatan_pegawai()
    {
        $this->db->select('assign_jabatan_pegawai.id, jabatan.nama as jabatan, assign_jabatan_pegawai.id_pegawai as nomor, assign_jabatan_pegawai.status');
        $this->db->from($this->table);
        $this->db->join('jabatan', 'assign_jabatan_pegawai.id_jabatan = jabatan.id');
        $this->db->where(['assign_jabatan_pegawai.status' => 1]);
        return $this->db->get();
    }


    public function filter($filter) 
    {
        $this->db->select('assign_jabatan_pegawai.id, assign_jabatan_pegawai.id_jabatan, assign_jabatan_pegawai.id_pegawai, pegawai.nama, assign_jabatan_pegawai.status');
        $this->db->from($this->table);
        $this->db->join('jabatan', 'assign_jabatan_pegawai.id_jabatan = jabatan.id');
        $this->db->join('pegawai', 'assign_jabatan_pegawai.id_pegawai = pegawai.id');
        $this->db->where($filter);
        return $this->db->get();
    }

    public function filterJabatan($filter) 
    {
        $this->db->select('assign_jabatan_pegawai.id_jabatan, pegawai.nomor');
        $this->db->from($this->table);
        $this->db->join('jabatan', 'assign_jabatan_pegawai.id_jabatan = jabatan.id');
        $this->db->join('pegawai', 'assign_jabatan_pegawai.id_pegawai = pegawai.id');
        $this->db->where($filter);
        return $this->db->get();
    }

    public function get_data_pegawai() 
    {
        $this->db->select('id');
        $this->db->from('pegawai');
        $this->db->where(['pegawai.status' => 1]);
        return $this->db->get()->result_array();
    }

    public function searchKepalaAsrama($search) 
    {
        $dataPegawai = $this->get_data_pegawai();
        $this->db->select();
        $this->db->from($this->table);
        $this->db->join('pegawai', 'assign_jabatan_pegawai.id_pegawai = pegawai.id');
        $this->db->where('assign_jabatan_pegawai.status',1);
        $this->db->where_in('assign_jabatan_pegawai.id_jabatan', [4,5,6,7]);
        $this->db->where_not_in('assign_jabatan_pegawai.id_pegawai',$dataPegawai);
        $this->db->like('pegawai.nomor', $search);

        return $this->db->get();
    }

    public function get_data_users() 
    {
        $this->db->select('nomor');
        $this->db->from('users');
        $this->db->where(['users.status' => 1]);
        return $this->db->get()->result_array();
    }

    public function search($search) 
    {
        $dataUsers = $this->get_data_users();
        $this->db->select();
        $this->db->from($this->table);
        $this->db->join('pegawai', 'assign_jabatan_pegawai.id_pegawai = pegawai.id');
        $this->db->where('assign_jabatan_pegawai.status',1);
        $this->db->where_not_in('pegawai.nomor',$dataUsers);
        $this->db->like('pegawai.nomor', $search);

        return $this->db->get();
    }

    public function save($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }

    public function delete($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }


}
