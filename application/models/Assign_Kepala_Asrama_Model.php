<?php

class Assign_Kepala_Asrama_Model extends CI_Model {
    
    protected $table = 'assign_kepala_asrama';

    protected $primaryKey = 'id';

    public function assign_kepala_asrama()
    {
        $this->db->select('assign_kepala_asrama.id, assign_kepala_asrama.id_asrama, assign_kepala_asrama.id_pegawai, assign_kepala_asrama.status, asrama.kode_asrama, pegawai.nomor');
        $this->db->from($this->table);
        $this->db->join('asrama', 'assign_kepala_asrama.id_asrama = asrama.id');
        $this->db->join('pegawai', 'assign_kepala_asrama.id_pegawai = pegawai.id');
        $this->db->where(['assign_kepala_asrama.status' => 1]);
        return $this->db->get();
    }


    public function filter($filter) 
    {
        $this->db->select()
        ->from($this->table)
        ->where($filter);

        return $this->db->get();
    }

    public function save($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }

    public function delete($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }


}
