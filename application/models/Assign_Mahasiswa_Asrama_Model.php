<?php

class Assign_Mahasiswa_Asrama_Model extends CI_Model {
    
    protected $table = 'assign_mahasiswa_asrama';

    protected $primaryKey = 'id';

    public function assign_mahasiswa_asrama()
    {
        $this->db->select('assign_mahasiswa_asrama.id, asrama.kode_asrama, mahasiswa.nomor as nim, assign_mahasiswa_asrama.status');
        $this->db->from($this->table);
        $this->db->join('mahasiswa', 'assign_mahasiswa_asrama.id_mahasiswa = mahasiswa.id');
        $this->db->join('asrama', 'assign_mahasiswa_asrama.id_asrama = asrama.id');
        $this->db->where(['assign_mahasiswa_asrama.status' => 1]);
        return $this->db->get();
    }


    public function filter($filter) 
    {
        $this->db->select('assign_mahasiswa_asrama.id, assign_mahasiswa_asrama.id_asrama, asrama.kode_asrama, assign_mahasiswa_asrama.id_mahasiswa, mahasiswa.nama, assign_mahasiswa_asrama.status');
        $this->db->from($this->table);
        $this->db->join('mahasiswa', 'assign_mahasiswa_asrama.id_mahasiswa = mahasiswa.id');
        $this->db->join('asrama', 'assign_mahasiswa_asrama.id_asrama = asrama.id');
        $this->db->where($filter);

        return $this->db->get();
    }

    public function save($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }

    public function delete($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }


}
