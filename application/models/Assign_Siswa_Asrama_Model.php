<?php

class Assign_Siswa_Asrama_Model extends CI_Model {
    
    protected $table = 'assign_siswa_asrama';

    protected $primaryKey = 'id';

    public function assign_siswa_asrama()
    {
        $this->db->select('assign_siswa_asrama.id, asrama.kode_asrama, siswa.nomor as nis, assign_siswa_asrama.status');
        $this->db->from($this->table);
        $this->db->join('siswa', 'assign_siswa_asrama.id_siswa = siswa.id');
        $this->db->join('asrama', 'assign_siswa_asrama.id_asrama = asrama.id');
        $this->db->where(['assign_siswa_asrama.status' => 1]);
        return $this->db->get();
    }


    public function filter($filter) 
    {
        $this->db->select('assign_siswa_asrama.id, assign_siswa_asrama.id_asrama, asrama.kode_asrama, assign_siswa_asrama.id_siswa, siswa.nama, assign_siswa_asrama.status');
        $this->db->from($this->table);
        $this->db->join('siswa', 'assign_siswa_asrama.id_siswa = siswa.id');
        $this->db->join('asrama', 'assign_siswa_asrama.id_asrama = asrama.id');
        $this->db->where($filter);
        return $this->db->get();
    }

    public function save($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }

    public function delete($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }


}
