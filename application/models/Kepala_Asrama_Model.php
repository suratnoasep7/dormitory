<?php

class Kepala_Asrama_Model extends CI_Model {
    
    protected $table = 'assign_kepala_asrama';

    protected $primaryKey = 'id';

    public function data_asrama($idPegawai)
    {

        $this->db->select('asrama.kode_asrama, mahasiswa.nomor, assign_mahasiswa_asrama.status');
        $this->db->from($this->table);
        $this->db->join('asrama', 'assign_kepala_asrama.id_asrama = asrama.id');
        $this->db->join('assign_mahasiswa_asrama', 'assign_mahasiswa_asrama.id_asrama = asrama.id');
        $this->db->join('mahasiswa', 'assign_mahasiswa_asrama.id_mahasiswa = mahasiswa.id');
        $this->db->where(['assign_mahasiswa_asrama.status' => 1, 'assign_kepala_asrama.id_pegawai' => $idPegawai]);
        return $this->db->get();
    }


    public function filter($filter) 
    {
        $this->db->select()
        ->from($this->table)
        ->where($filter);

        return $this->db->get();
    }
    
    public function save($data)
    {
        return $this->db->insert('pelanggaran', $data);
    }

    public function saveReward($data)
    {
        return $this->db->insert('reward', $data);
    }

    public function update($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }

    public function delete($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }


}
