<?php

class Kepala_Asrama_Siswa_Model extends CI_Model {
    
    protected $table = 'assign_kepala_asrama';

    protected $primaryKey = 'id';

    public function data_asrama($idPegawai)
    {

        $this->db->select('asrama.kode_asrama, siswa.nomor, assign_siswa_asrama.status');
        $this->db->from($this->table);
        $this->db->join('asrama', 'assign_kepala_asrama.id_asrama = asrama.id');
        $this->db->join('assign_siswa_asrama', 'assign_siswa_asrama.id_asrama = asrama.id');
        $this->db->join('siswa', 'assign_siswa_asrama.id_siswa = siswa.id');
        $this->db->where(['assign_siswa_asrama.status' => 1, 'assign_kepala_asrama.id_pegawai' => $idPegawai]);
        return $this->db->get();
    }


    public function filter($filter) 
    {
        $this->db->select()
        ->from($this->table)
        ->where($filter);

        return $this->db->get();
    }
    
    public function save($data)
    {
        return $this->db->insert('pelanggaran', $data);
    }

    public function saveReward($data)
    {
        return $this->db->insert('reward', $data);
    }

    public function update($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }

    public function delete($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }


}
