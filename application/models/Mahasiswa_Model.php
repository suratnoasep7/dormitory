<?php

class Mahasiswa_Model extends CI_Model {
    
    protected $table = 'mahasiswa';

    protected $primaryKey = 'id';

    public function mahasiswa()
    {
        $this->db->select('id, nomor, nama, tempat_lahir, tanggal_lahir, alamat, nomor_telepon, jenis_kelamin, status');
        $this->db->from($this->table);
        $this->db->where(['status' => 1]);
        return $this->db->get();
    }


    public function filter($filter) 
    {
        $this->db->select()
        ->from($this->table)
        ->where($filter);

        return $this->db->get();
    }

    public function get_assign_mahasiswa_asrama() 
    {
        $this->db->select('id_mahasiswa');
        $this->db->from('assign_siswa_mahasiswa');
        $this->db->where(['assign_siswa_mahasiswa.status' => 1]);
        return $this->db->get()->result_array();
    }

    public function search($search) 
    {
        $dataAssignMahasiswaAsrama = $this->get_assign_mahasiswa_asrama();

        $this->db->select()
        ->from($this->table)
        ->join('assign_mahasiswa_asrama', 'assign_mahasiswa_asrama.id_mahasiswa = mahasiswa.id')
        ->where('mahasiswa.status',1)
        ->where_not_in('mahasiswa.id',$dataAssignMahasiswaAsrama);
        if($search != null) {
            $this->db->like('nomor',$search)
            ->or_like('nama', $search);
        }
        return $this->db->get();   
    }

    public function save($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }

    public function delete($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }


}
