<?php

class Pelanggaran_Kepala_Model extends CI_Model {
    
    protected $table = 'pelanggaran_kepala';

    protected $primaryKey = 'id';

    public function pelanggaran_kepala()
    {
        $this->db->select('pelanggaran.id, pelanggaran_kepala.id_pelanggaran, pelanggaran_kepala.tindakan_pelanggaran as tindakan_puket_tiga, pelanggaran_kepala.masa_hukuman_pelanggaran as masa_hukuman, pelanggaran_kepala.keterangan_pelanggaran as keterangan, pelanggaran.tindakan_pelanggaran as tindakan, pelanggaran.respon_pelanggaran as respon, pelangggaran.tanggal_pelanggaran as tanggal_tindakan, pelanggaran.keterangan_pelanggaran as keterangan_tindakan, kategori_pelanggaran.nama as deskripsi, mahasiswa.nama as nama_mahasiswa, pelanggaran.status');
        $this->db->from($this->table);
        $this->db->join('pelanggaran', 'pelanggaran_kepala.id_pelanggaran = pelanggaran.id');
        $this->db->join('kategori_pelanggaran', 'pelanggaran.id_pelanggaran = kategori_pelanggaran.id');
        $this->db->join('mahasiswa', 'pelanggaran.nomor = mahasiswa.nomor');
        return $this->db->get();
    }

    public function pelanggaran_kepala_siswa()
    {
        $this->db->select('id, id_pelanggaran, tindakan_pelanggaran as tindakan_pks_tiga, masa_hukuman_pelanggaran as masa_hukuman, keterangan_pelanggaran as keterangan, pelanggaran.tindakan_pelanggaran as tindakan, pelanggaran.respon_pelanggaran as respon, pelangggaran.tanggal_pelanggaran as tanggal_tindakan, pelanggaran.keterangan_pelanggaran as keterangan_tindakan, kategori_pelanggaran.nama as deskripsi, mahasiswa.nama as nama_mahasiswa, pelanggaran.status');
        $this->db->from($this->table);
        $this->db->join('pelanggaran', 'pelanggaran_kepala.id_pelanggaran = pelanggaran.id');
        $this->db->join('kategori_pelanggaran', 'pelanggaran.id_pelanggaran = kategori_pelanggaran.id');
        $this->db->join('siswa', 'pelanggaran.nomor = siswa.nomor');
        return $this->db->get();
    }

    
    public function save($data)
    {
        return $this->db->insert($this->table, $data);
    }

}
