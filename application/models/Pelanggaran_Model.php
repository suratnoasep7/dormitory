<?php

class Pelanggaran_Model extends CI_Model {
    
    protected $table = 'pelanggaran';

    protected $primaryKey = 'id';

    public function pelanggaran()
    {
        $this->db->select('id, nama_pelanggaran, status');
        $this->db->from($this->table);
        $this->db->where(['status' => 1]);
        return $this->db->get();
    }

    public function dataPelanggaran()
    {
        $this->db->select('kategori_pelanggaran.id, kategori_pelanggaran.nama_pelanggaran, tindakan_pelanggaran, respon_pelanggaran, keterangan_pelanggaran, tanggal_pelanggaran, pelanggaran.status, pelanggaran.nomor');
        $this->db->from($this->table);
        $this->db->join('kategori_pelanggaran', 'pelanggaran.id_pelanggaran = kategori_pelanggaran.id');
        $this->db->where(['pelanggaran.status' => 1]);
        return $this->db->get();
    }

    public function pelanggaranMahasiswa()
    {
        $this->db->select('kategori_pelanggaran.id, kategori_pelanggaran.nama_pelanggaran, tindakan_pelanggaran, respon_pelanggaran, keterangan_pelanggaran, tanggal_pelanggaran, mahasiswa.nama as nama_mahasiswa, pelanggaran.status, pelanggaran.nomor');
        $this->db->from($this->table);
        $this->db->join('kategori_pelanggaran', 'pelanggaran.id_pelanggaran = kategori_pelanggaran.id');
        $this->db->join('mahasiswa', 'mahasiswa.nomor = pelanggaran.nomor');
        $this->db->where(['pelanggaran.status' => 1]);
        return $this->db->get();
    }

    public function pelanggaranSiswa()
    {
        $this->db->select('kategori_pelanggaran.id, kategori_pelanggaran.nama_pelanggaran, tindakan_pelanggaran, respon_pelanggaran, keterangan_pelanggaran, tanggal_pelanggaran, siswa.nama as nama_siswa, pelanggaran.status');
        $this->db->from($this->table);
        $this->db->join('kategori_pelanggaran', 'pelanggaran.id_pelanggaran = kategori_pelanggaran.id');
        $this->db->join('siswa', 'siswa.nomor = pelanggaran.nomor');
        $this->db->where(['pelanggaran.status' => 1]);
        return $this->db->get();
    }


    public function filter($filter) 
    {
        $this->db->select()
        ->from($this->table)
        ->where($filter);

        return $this->db->get();
    }
    
    public function save($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function search($search) 
    {
        $this->db->select()
        ->from('kategori_pelanggaran')
        ->where('status',1);
        if($search != null) {
            $this->db->like('nama_pelanggaran',$search);
        }
        return $this->db->get();   
    }

    public function searchReward($search) 
    {
        $this->db->select()
        ->from('kategori_reward')
        ->where('status',1);
        if($search != null) {
            $this->db->like('nama_reward',$search);
        }
        return $this->db->get();   
    }

    public function update($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }

    public function delete($id, $update)
    {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $update);
    }


}
