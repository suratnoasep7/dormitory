<?php

class Reward_Model extends CI_Model {
    
    protected $table = 'reward';

    protected $primaryKey = 'id';

    public function rewardMahasiswa()
    {
        $this->db->select('kategori_reward.id, kategori_reward.nama_reward, tindakan_reward, respon_reward, keterangan_reward, tanggal_reward, mahasiswa.nama as nama_mahasiswa, reward.status');
        $this->db->from($this->table);
        $this->db->join('kategori_reward', 'reward.id_reward = kategori_reward.id');
        $this->db->join('mahasiswa', 'mahasiswa.nomor = reward.nomor');
        $this->db->where(['reward.status' => 1]);
        return $this->db->get();
    }

    public function rewardSiswa()
    {
        $this->db->select('kategori_reward.id, kategori_reward.nama_reward, tindakan_reward, respon_reward, keterangan_reward, tanggal_reward, siswa.nama as nama_siswa, reward.status, reward.nomor');
        $this->db->from($this->table);
        $this->db->join('kategori_reward', 'reward.id_reward = kategori_reward.id');
        $this->db->join('siswa', 'siswa.nomor = reward.nomor');
        $this->db->where(['reward.status' => 1]);
        return $this->db->get();
    }



}
