<style type="text/css">
  .detail_form_pelanggaran {
    width: 100%;
  }
  .detail_form_reward {
    width: 100%;
  }
</style>
<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Asrama</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Asrama</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <table id="tbl_kepala_asrama_siswa" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Kode Asrama</th>
                  <th>NIS</th>
                  <td>Status</td>
                  <td></td>
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
    <div class="modal fade" id="modalPilihSiswa">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Pilih Siswa</h4>
          </div>
          <div class="modal-body">
            <form id="kepala_asrama_siswa" method="post" name="kepala_asrama_siswa">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="tipe">Tipe</label>
                    <input type="hidden" name="nomor" value="" id="nomor">
                    <select class="form-control" name="tipe" onchange="getTipe(this)">
                      <option>-- PILIH --</option>
                      <option value="0">Pelanggaran</option>
                      <option value="1">Reward</option>
                    </select>
                  </div>    
                </div>
                <div class="detail_form_pelanggaran" style="display: none">
                  <div class="col-md-12">
                    <div class="row">
                      
                    
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="pelanggaran">Pelanggaran</label>
                          <select class="form-control select2" name="pelanggaran" required="" id="pelanggaran" style="width: 100%"></select>  
                        </div>
                        <div class="form-group">
                          <label for="tanggal_pelanggaran">Tanggal Pelanggaran</label>
                          <input type="date" name="tanggal_pelanggaran" class="form-control" required="" autocomplete="">
                        </div>
                        <div class="form-group">
                          <label for="respon_pelanggaran">Respon Pelanggaran</label>
                          <textarea class="form-control" name="respon_pelanggaran" required="" autocomplete=""></textarea>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="tindakan_pelanggaran">Tindakan Pelanggaran</label>
                          <textarea class="form-control" name="tindakan_pelanggaran" required="" autocomplete=""></textarea>
                        </div>
                        <div class="form-group">
                          <label for="keterangan">Keterangan</label>
                          <textarea class="form-control" name="keterangan" required="" autocomplete=""></textarea>
                        </div>
                        <div class="form-group">
                          <label for="status">Status</label>
                          <select class="form-control" name="status" required="" autocomplete>
                            <option>-- PILIH --</option>
                            <option value="0">Selesai</option>
                            <option value="1">Naik Ke Puket 3</option>
                          </select>
                        </div>
                      </div>  
                    </div>
                  </div>
                </div>
                <div class="detail_form_reward" style="display: none">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="reward">Reward</label>
                          <select class="form-control select2" name="reward" required="" id="reward" style="width: 100%"></select>  
                        </div>
                        <div class="form-group">
                          <label for="tanggal_reward">Tanggal Reward</label>
                          <input type="date" name="tanggal_reward" class="form-control" required="" autocomplete="">
                        </div>
                        <div class="form-group">
                          <label for="respon_reward">Respon Reward</label>
                          <textarea class="form-control" name="respon_reward" required="" autocomplete=""></textarea>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="tindakan_reward">Tindakan Reward</label>
                          <textarea class="form-control" name="tindakan_reward" required="" autocomplete=""></textarea>
                        </div>
                        <div class="form-group">
                          <label for="keterangan">Keterangan</label>
                          <textarea class="form-control" name="keterangan" required="" autocomplete=""></textarea>
                        </div>
                        <div class="form-group">
                          <label for="status">Status</label>
                          <select class="form-control" name="status" required="" autocomplete>
                            <option>-- PILIH --</option>
                            <option value="0">Selesai</option>
                            <option value="1">Naik Ke Puket 3</option>
                          </select>
                        </div>
                      </div>  
                    </div>  
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btn_tambah_kepala_asrama_siswa">Save changes</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->