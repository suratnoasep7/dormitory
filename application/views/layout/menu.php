<nav class="mt-2" style="border-bottom: 1px solid #4f5962">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <?php 
      $this->load->helper('menu');
      $data_menu = get_menu($this->session->userdata('id_jabatan'));

      foreach ($data_menu as $key => $value) {
        
    ?>
      <li class="nav-item <?php echo (count(get_sub_menu($value['id'])) > 1) ? "has-treeview" : "" ?>">
        <a href="<?php echo base_url($value['link_menu']) ?>" class="nav-link">
          <i class="nav-icon <?php echo $value['icon_menu'] ?>"></i>
          <p>
            <?php echo $value['nama_menu'] ?>
            <?php if(count(get_sub_menu($value['id'])) > 1) : ?>
              <i class="fas fa-angle-left right"></i>
            <?php endif; ?>
          </p>
        </a>
        <?php $data_sub_menu = get_sub_menu($value['id']); ?>
        <?php  if(count($data_sub_menu) > 1) : ?>
          <ul class="nav nav-treeview">
            <?php
            foreach ($data_sub_menu as $k => $val) {
            ?>
            <li class="nav-item">
              <a href="<?php echo base_url($val['link_menu']) ?>" class="nav-link">
                <i class="<?php echo $val['icon_menu'] ?> nav-icon"></i>
                <p><?php echo $val['nama_menu'] ?></p>
              </a>
            </li>
            <?php
            }
            ?>
          </ul>
        <?php endif; ?>
      </li>
    <?php
      }
    ?>
  </ul>
</nav>