<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Pelanggaran</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Pelanggaran</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <table id="tbl_pelanggaran_kepala" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Nama Mahasiswa</th>
                  <td>Kategori</td>
                  <td>Deskripsi</td>
                  <td>Tindakan</td>
                  <td>Respon</td>
                  <td>Tanggal Tindakan</td>
                  <td>Keterangan Tindakan</td>
                  <td>Status</td>
                  <td>Tindakan PKS III</td>
                  <td>Masa Hukuman</td>
                  <td>Keterangan</td>
                  <td></td>
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
    <div class="modal fade" id="modalTambahPelanggaranKepala">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"></h4>
          </div>
          <div class="modal-body">
            <form id="pelanggaran_kepala" method="post" name="pelanggaran_kepala">
              <div class="form-group">
                <input type="hidden" name="mode" value="tambah" id="mode">
                <input type="hidden" name="id" id="id">
                <label for="tindakan_puket_tiga">Tindakan PKS 3</label>
                <input type="text" class="form-control" id="tindakan_puket_tiga" name="tindakan_puket_tiga" autocomplete="off" required="" >
              </div>
              <div class="form-group">
                <label for="masa_hukuman">Masa Hukuman</label>
                <input type="text" class="form-control" id="masa_hukuman" name="masa_hukuman" autocomplete="off" required="" >
              </div>
              <div class="form-group">
                <label for="keterangan">Keterangan</label>
                <textarea class="form-control" name="keterangan" id="keterangan" required=""></textarea>
              </div>    
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btn_tambah_pelanggaran_kepala">Save changes</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->