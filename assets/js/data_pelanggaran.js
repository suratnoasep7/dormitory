var table = $('#tbl_pelangaran').DataTable({
    "ajax": {
        "type": "GET",
        "url": base_url + 'api/data_pelanggaran',
        "beforeSend" : function(xhr) {
          xhr.setRequestHeader('Authorization',access_token);
        }
    },
    "columns": [
        { "data": "nomor" },
        { "data": "nama_pelanggaran" },
        { "data": "tindakan_pelanggaran" },
        { "data": "respon_pelanggaran" },
        { "data": "keterangan_pelanggaran" },
        { "data": "tanggal_pelanggaran" },
        { 
            "data": null, 
            render:function(data) {
                if(data.status == "1") {
                    return "Naik Ke Puket 3";
                } else {
                    return "Selesai";
                }     
            } 
        }
    ]
});

var table = $('#tbl_data_pelangaran').DataTable({
    "ajax": {
        "type": "GET",
        "url": base_url + 'api/data_pelanggaran',
        "beforeSend" : function(xhr) {
          xhr.setRequestHeader('Authorization',access_token);
        }
    },
    "columns": [
        { "data": "nomor" },
        { "data": "nama_pelanggaran" },
        { "data": "tindakan_pelanggaran" },
        { "data": "respon_pelanggaran" },
        { "data": "keterangan_pelanggaran" },
        { "data": "tanggal_pelanggaran" },
        { 
            "data": null, 
            render:function(data) {
                if(data.status == "1") {
                    return "Naik Ke PKS 3";
                } else {
                    return "Selesai";
                }     
            } 
        }
    ]
});

