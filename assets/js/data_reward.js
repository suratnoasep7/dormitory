var table = $('#tbl_reward').DataTable({
    "ajax": {
        "type": "GET",
        "url": base_url + 'api/data_reward',
        "beforeSend" : function(xhr) {
          xhr.setRequestHeader('Authorization',access_token);
        }
    },
    "columns": [
        { "data": "nomor" },
        { "data": "nama_reward" },
        { "data": "tindakan_reward" },
        { "data": "respon_reward" },
        { "data": "keterangan_reward" },
        { "data": "tanggal_reward" },
        { 
            "data": null, 
            render:function(data) {
                if(data.status == "1") {
                    return "Naik Ke Puket 3";
                } else {
                    return "Selesai";
                }     
            } 
        }
    ]
});

var table = $('#tbl_data_reward').DataTable({
    "ajax": {
        "type": "GET",
        "url": base_url + 'api/data_reward',
        "beforeSend" : function(xhr) {
          xhr.setRequestHeader('Authorization',access_token);
        }
    },
    "columns": [
        { "data": "nomor" },
        { "data": "nama_reward" },
        { "data": "tindakan_reward" },
        { "data": "respon_reward" },
        { "data": "keterangan_reward" },
        { "data": "tanggal_reward" },
        { 
            "data": null, 
            render:function(data) {
                if(data.status == "1") {
                    return "Naik Ke PKS 3";
                } else {
                    return "Selesai";
                }     
            } 
        }
    ]
});

