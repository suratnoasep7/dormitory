var table = $('#tbl_kategori_pelanggaran').DataTable({
    "ajax": {
        "type": "GET",
        "url": base_url + 'api/kategori_pelanggaran',
        "beforeSend" : function(xhr) {
          xhr.setRequestHeader('Authorization',access_token);
        }
    },
    "columns": [
        { "data": "nama_pelanggaran" },
        { 
            "data": null, 
            render:function(data) {
                if(data.status == "1") {
                    return "Aktif";
                } else {
                    return "Tidak Aktif";
                }     
            } 
        },
        { "data": null,
            "render": function (data) {
                return '<a href="#" onclick="editKategoriPelanggaran(\'' + data.id + '\')"><i class="fa fa-pencil-alt"></i></a> <a href="#" onclick="deleteKategoriPelanggaran(\'' + data.id + '\')"><i class="far fa-trash-alt"></i></a>';
            }
        }
    ]
});
$('#btn_tambah_kategori_pelanggaran').click(function() {

    var $form = $("#kategori_pelanggaran");
    var kategori_pelanggaran = getDataForm($form);

    if(kategori_pelanggaran.nama_pelanggaran == "" ) {
        responseError();
    } else {
        if(kategori_pelanggaran.mode == "tambah") {
            var tambahKategoriPelanggaran = ajaxPost("POST","api/kategori_pelanggaran",kategori_pelanggaran);
            if(!tambahKategoriPelanggaran.status) {
                responseError();
            }
            responseSuccess(tambahKategoriPelanggaran.message);
        } else {
            var updateKategoriPelanggaran = ajaxPost("PUT","api/kategori_pelanggaran",kategori_pelanggaran);
            if(!updateKategoriPelanggaran.status) {
                responseError();
            } 
            responseSuccess(updateKategoriPelanggaran.message);
        }    
    }
});

$('#btn_delete_kategori_pelanggaran').click(function() {

    var idKategoriPelanggaran = $("#id_delete_kategori_pelanggaran").val();
    if(idKategoriPelanggaran == null || idKategoriPelanggaran == "") {
        responseError();
    }
    var kategori_pelanggaran = {
        id : idKategoriPelanggaran
    }
    var deleteKategoriPelanggaran = ajaxPost("DELETE","api/kategori_pelanggaran",kategori_pelanggaran);
    if(!deleteKategoriPelanggaran.status) {
        responseError();
    }
    responseSuccess(deleteKategoriPelanggaran.message);
});

function tambahKategoriPelanggaran() {
    $('#modalTambahKategoriPelanggaran').modal('show');
    $('#modalTambahKategoriPelanggaran .modal-title').text('Tambah Data Kategori Pelanggaran');
}

function editKategoriPelanggaran(id) 
{
    $('#modalTambahKategoriPelanggaran').modal('show');
    $('#mode').val("edit");
    $('#modalTambahKategoriPelanggaran .modal-title').text('Edit Data Kategori Pelanggaran');

    var data = {
        id: id
    };

    var kategori_pelanggaran = ajaxPost("POST","api/kategori_pelanggaran/get_data_kategori_pelanggaran",data);  
    if(kategori_pelanggaran.data.length > 0) {
        for (var i = 0; i < kategori_pelanggaran.data.length; i++) {
            $('#id').val(kategori_pelanggaran.data[i].id);
            $('#nama_pelanggaran').val(kategori_pelanggaran.data[i].nama_pelanggaran);
            $('#status').val(kategori_pelanggaran.data[i].status);
        }
    }
}

function deleteKategoriPelanggaran(id) 
{
    $('#modalDeleteKategoriPelanggaran').modal('show');
    $('#mode').val("edit");
    $('#modalDeleteKategoriPelanggaran .modal-title').text('Delete Data Kategori Pelanggaran');
    $('#id_delete_kategori_pelanggaran').val(id);
}

function responseSuccess(message) {
    $('#modalTambahKategoriPelanggaran').modal('hide');
    $('#modalDeleteKategoriPelanggaran').modal('hide');
    $('#id_delete_kategori_pelanggaran').val();
    $("#kategori_pelanggaran")[0].reset();
    $("#mode").val("tambah");
    toastr.success(message);
    setTimeout(function(){ location.reload(); }, 3000);   
}
function responseError() 
{
    toastr.error('Data Di Isi Dengan Baik dan Benar');
}
