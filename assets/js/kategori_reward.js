var table = $('#tbl_kategori_reward').DataTable({
    "ajax": {
        "type": "GET",
        "url": base_url + 'api/kategori_reward',
        "beforeSend" : function(xhr) {
          xhr.setRequestHeader('Authorization',access_token);
        }
    },
    "columns": [
        { "data": "nama_reward" },
        { 
            "data": null, 
            render:function(data) {
                if(data.status == "1") {
                    return "Aktif";
                } else {
                    return "Tidak Aktif";
                }     
            } 
        },
        { "data": null,
            "render": function (data) {
                return '<a href="#" onclick="editKategoriReward(\'' + data.id + '\')"><i class="fa fa-pencil-alt"></i></a> <a href="#" onclick="deleteKategoriReward(\'' + data.id + '\')"><i class="far fa-trash-alt"></i></a>';
            }
        }
    ]
});
$('#btn_tambah_kategori_reward').click(function() {

    var $form = $("#kategori_reward");
    var kategori_reward = getDataForm($form);

    if(kategori_reward.nama_pelanggaran == "" ) {
        responseError();
    } else {
        if(kategori_reward.mode == "tambah") {
            var tambahKategoriReward = ajaxPost("POST","api/kategori_reward",kategori_reward);
            if(!tambahKategoriReward.status) {
                responseError();
            }
            responseSuccess(tambahKategoriReward.message);
        } else {
            var updateKategoriReward = ajaxPost("PUT","api/kategori_reward",kategori_reward);
            if(!updateKategoriReward.status) {
                responseError();
            } 
            responseSuccess(updateKategoriReward.message);
        }    
    }
});

$('#btn_delete_kategori_reward').click(function() {

    var idKategoriReward = $("#id_delete_kategori_reward").val();
    if(idKategoriReward == null || idKategoriReward == "") {
        responseError();
    }
    var kategori_reward = {
        id : idKategoriReward
    }
    var deleteKategoriReward = ajaxPost("DELETE","api/kategori_reward",kategori_reward);
    if(!deleteKategoriReward.status) {
        responseError();
    }
    responseSuccess(deleteKategoriReward.message);
});

function tambahKategoriReward() {
    $('#modalTambahKategoriReward').modal('show');
    $('#modalTambahKategoriReward .modal-title').text('Tambah Data Kategori Reward');
}

function editKategoriReward(id) 
{
    $('#modalTambahKategoriReward').modal('show');
    $('#mode').val("edit");
    $('#modalTambahKategoriReward .modal-title').text('Edit Data Kategori Reward');

    var data = {
        id: id
    };

    var kategori_reward = ajaxPost("POST","api/kategori_reward/get_data_kategori_reward",data);  
    if(kategori_reward.data.length > 0) {
        for (var i = 0; i < kategori_reward.data.length; i++) {
            $('#id').val(kategori_reward.data[i].id);
            $('#nama_reward').val(kategori_reward.data[i].nama_reward);
            $('#status').val(kategori_reward.data[i].status);
        }
    }
}

function deleteKategoriReward(id) 
{
    $('#modalDeleteKategoriReward').modal('show');
    $('#mode').val("edit");
    $('#modalDeleteKategoriReward .modal-title').text('Delete Data Kategori Reward');
    $('#id_delete_kategori_reward').val(id);
}

function responseSuccess(message) {
    $('#modalTambahKategoriReward').modal('hide');
    $('#modalDeleteKategoriReward').modal('hide');
    $('#id_delete_kategori_reward').val();
    $("#kategori_reward")[0].reset();
    $("#mode").val("tambah");
    toastr.success(message);
    setTimeout(function(){ location.reload(); }, 3000);   
}
function responseError() 
{
    toastr.error('Data Di Isi Dengan Baik dan Benar');
}
