$("#pelanggaran").select2({
    theme: 'bootstrap',
    ajax: {
        url: base_url + 'api/pelanggaran/search_pelanggaran',
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        "beforeSend" : function(xhr) {
          xhr.setRequestHeader('Authorization',access_token);
        },
        processResults: function (data) {
            return {
                results: $.map(data.data, function (item) {
                    return {
                        text: item.nama_pelanggaran,
                        id: item.id
                    }
                })
            };
        }
    }
});

$("#reward").select2({
    theme: 'bootstrap',
    ajax: {
        url: base_url + 'api/pelanggaran/search_reward',
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        "beforeSend" : function(xhr) {
          xhr.setRequestHeader('Authorization',access_token);
        },
        processResults: function (data) {
            return {
                results: $.map(data.data, function (item) {
                    return {
                        text: item.nama_reward,
                        id: item.id
                    }
                })
            };
        }
    }
});

var table = $('#tbl_kepala_asrama_siswa').DataTable({
    "ajax": {
        "type": "GET",
        "url": base_url + 'api/kepala_asrama_siswa',
        "beforeSend" : function(xhr) {
          xhr.setRequestHeader('Authorization',access_token);
        }
    },
    "columns": [
        { "data": "kode_asrama" },
        { "data": "nomor" },
        { 
            "data": null, 
            render:function(data) {
                if(data.status == "1") {
                    return "Aktif";
                } else {
                    return "Tidak Aktif";
                }     
            } 
        },
        { "data": null,
            "render": function (data) {
                return '<a href="#" onclick="pilihSiswa(\'' + data.nomor + '\')"><i class="fa fa-check"></i></a>';
            }
        }
    ]
});

$('#btn_tambah_kepala_asrama_siswa').click(function() {

    var $form = $("#kepala_asrama_siswa");
    var kepala_asrama_siswa = getDataForm($form);
    if(kepala_asrama_siswa.tipe == "" ) {
        responseError();
    } else {
        var tambahDataAsrama = ajaxPost("POST","api/kepala_asrama_siswa",kepala_asrama_siswa);
        if(!tambahDataAsrama.status) {
            responseError();
        }
        responseSuccess(tambahDataAsrama.message);    
    }
});
function pilihSiswa(nomor) {
    $('#modalPilihSiswa').modal('show');
    $('#modalPilihSiswa .modal-title').text('Pilih Siswa');
    $('#nomor').val(nomor);
}
function getTipe(data) {
    if(data.value == "0") {
        $('.detail_form_pelanggaran').show();
        $('.detail_form_reward').hide();
    } else {
        $('.detail_form_pelanggaran').hide();
        $('.detail_form_reward').show();
    }
}
function responseSuccess(message) {
    $('#modalPilihSiswa').modal('hide');
    $('#nomor').val();
    $("#kepala_asrama_siswa")[0].reset();
    toastr.success(message);
    setTimeout(function(){ location.reload(); }, 3000);   
}
function responseError() 
{
    toastr.error('Data Di Isi Dengan Baik dan Benar');
}