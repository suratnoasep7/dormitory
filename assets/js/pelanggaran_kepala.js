var table = $('#tbl_pelangaran').DataTable({
    "ajax": {
        "type": "GET",
        "url": base_url + 'api/pelanggaran_kepala',
        "beforeSend" : function(xhr) {
          xhr.setRequestHeader('Authorization',access_token);
        }
    },
    "columns": [
        { "data": "nama_mahasiswa" },
        { "data": "kategori" },
        { "data": "deskripsi" },
        { "data": "tindakan" },
        { "data": "respon" },
        { "data": "tanggal_tindakan" },
        { "data": "keterangan_tindakan" },
        { 
            "data": null, 
            render:function(data) {
                if(data.status == "1") {
                    return "Naik Ke Puket 3";
                } else {
                    return "Selesai";
                }     
            } 
        },
        { "data": "tindakan_puket_tiga" },
        { "data": "masa_hukuman" },
        { "data": "keterangan" },
        { "data": null,
            "render": function (data) {
                return '<a href="#" onclick="editDataPelanggaranKepala(\'' + data.id + '\')"><i class="fa fa-pencil-alt"></i></a>';
            }
        }
    ]
});
$('#btn_tambah_pelanggaran_kepala').click(function() {

    var $form = $("#pelanggaran_kepala");
    var pelanggaran_kepala = getDataForm($form);

    if(pelanggaran_kepala.tindakan_puket_tiga == "" || pelanggaran_kepala.masa_hukuman == "" ) {
        responseError();
    } else {
        var tambahPelanggaranKepala = ajaxPost("POST","api/pelanggaran_kepala",pelanggaran_kepala);
        if(!tambahPelanggaranKepala.status) {
            responseError();
        }
        responseSuccess(tambahPelanggaranKepala.message);    
    }
});


function editDataPelanggaranKepala(id) {
    $('#modalTambahPelanggaranKepala').modal('show');
    $('#modalTambahPelanggaranKepala .modal-title').text('Tambah Data Pelanggaran');
    $('#id').val(id);
}


function responseSuccess(message) {
    $('#modalTambahPelanggaranKepala').modal('hide');
    $("#pelanggaran_kepala")[0].reset();
    $("#mode").val("tambah");
    toastr.success(message);
    setTimeout(function(){ location.reload(); }, 3000);   
}
function responseError() 
{
    toastr.error('Data Di Isi Dengan Baik dan Benar');
}
