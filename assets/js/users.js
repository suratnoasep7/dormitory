$("#nomor").select2({
    theme: 'bootstrap',
    ajax: {
        url: base_url + 'api/assign_jabatan_pegawai/get_assign_jabatan',
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        "beforeSend" : function(xhr) {
          xhr.setRequestHeader('Authorization',access_token);
        },
        processResults: function (data) {
            return {
                results: $.map(data.data, function (item) {
                    return {
                        text: item.nama,
                        id: item.id
                    }
                })
            };
        }
    }
});

var table = $('#tbl_users').DataTable({
    "ajax": {
        "type": "GET",
        "url": base_url + 'api/users',
        "beforeSend" : function(xhr) {
          xhr.setRequestHeader('Authorization',access_token);
        }
    },
    "columns": [
        { "data": "jabatan" },
        { "data": "nomor" },
        { 
            "data": null, 
            render:function(data) {
                if(data.status == "1") {
                    return "Aktif";
                } else {
                    return "Tidak Aktif";
                }     
            } 
        },
        { "data": null,
            "render": function (data) {
                return '<a href="#" onclick="editUsers(\'' + data.id + '\')"><i class="fa fa-pencil-alt"></i></a> <a href="#" onclick="deleteUsers(\'' + data.id + '\')"><i class="far fa-trash-alt"></i></a>';
            }
        }
    ]
});
$('#btn_tambah_users').click(function() {

    var $form = $("#users");
    var users = getDataForm($form);

    if(users.nomor == "" || users.password == "" || users.status == "") {
        responseError();
    } else {
        var data = {
            nomor: users.nomor
        };
        var assign_jabatan_pegawai = ajaxPost("POST","api/assign_jabatan_pegawai/get_data_assign_jabatan_pegawai_nomor",data);
        users.id_jabatan = assign_jabatan_pegawai.data[0].id_jabatan;
        users.nomor = assign_jabatan_pegawai.data[0].nomor;

        if(users.mode == "tambah") {
            var tambahUsers = ajaxPost("POST","api/users",users);
            if(!tambahUsers.status) {
                responseError();
            }
            responseSuccess(tambahUsers.message);
        } else {
            var updateUsers = ajaxPost("PUT","api/users",users);
            if(!updateUsers.status) {
                responseError();
            } 
            responseSuccess(updateUsers.message);
        }    
    }
});

$('#btn_delete_users').click(function() {

    var idUsers = $("#id_delete_users").val();
    if(idUsers == null || idUsers == "") {
        responseError();
    }
    var users = {
        id : idUsers
    }
    var deleteUsers = ajaxPost("DELETE","api/users",users);
    if(!deleteUsers.status) {
        responseError();
    }
    responseSuccess(deleteUsers.message);
});

function tambahUsers() {
    $('#modalTambahUsers').modal('show');
    $('#modalTambahUsers .modal-title').text('Tambah Data Users');
}

function editUsers(id) 
{
    $('#modalTambahUsers').modal('show');
    $('#mode').val("edit");
    $('#modalTambahUsers .modal-title').text('Edit Data Users');

    var data = {
        id: id
    };

    var users = ajaxPost("POST","api/users/get_data_users",data);  
    if(users.data.length > 0) {
        for (var i = 0; i < users.data.length; i++) {
            $('#id').val(users.data[i].id);
            var dataUsers = {
                id: users.data[i].pegawai_id,
                text: users.data[i].nama
            };

            var newOptionUsers = new Option(dataUsers.text, dataUsers.id, false, false);
            $('#nomor').append(newOptionUsers).trigger('change');
            $('#id_jabatan').val(users.data[i].id_jabatan);
            $('#status').val(users.data[i].status);
        }
    }
}

function deleteUsers(id) 
{
    $('#modalDeleteUsers').modal('show');
    $('#mode').val("edit");
    $('#modalDeleteUsers .modal-title').text('Delete Data Users');
    $('#id_delete_users').val(id);
}

function responseSuccess(message) {
    $('#modalTambahUsers').modal('hide');
    $('#modalDeleteUsers').modal('hide');
    $('#id_delete_users').val();
    $("#users")[0].reset();
    $('#id_jabatan').val();
    $("#mode").val("tambah");
    $("#nomor").select2("val", "");
    toastr.success(message);
    setTimeout(function(){ location.reload(); }, 3000);   
}
function responseError() 
{
    toastr.error('Data Di Isi Dengan Baik dan Benar');
}
