/*
 Navicat Premium Data Transfer

 Source Server         : LOCALMYSQL
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : dbdormitory

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 11/05/2020 14:59:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for asrama
-- ----------------------------
DROP TABLE IF EXISTS `asrama`;
CREATE TABLE `asrama`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode_kategori_asrama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kode_asrama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `hall` enum('A','B') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nomor_kamar` bigint(20) NULL DEFAULT NULL,
  `lantai` enum('1','2','3') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT NULL,
  `created_by` bigint(20) NULL DEFAULT NULL,
  `created_date` timestamp(0) NULL DEFAULT current_timestamp(),
  `updated_by` bigint(20) NULL DEFAULT NULL,
  `updated_date` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` bigint(20) NULL DEFAULT NULL,
  `deleted_date` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for assign_jabatan_pegawai
-- ----------------------------
DROP TABLE IF EXISTS `assign_jabatan_pegawai`;
CREATE TABLE `assign_jabatan_pegawai`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_jabatan` bigint(20) NULL DEFAULT NULL,
  `nomor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyblob NULL,
  `created_by` bigint(20) NULL DEFAULT NULL,
  `created_date` timestamp(0) NULL DEFAULT current_timestamp(),
  `updated_by` bigint(20) NULL DEFAULT NULL,
  `updated_date` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` bigint(20) NULL DEFAULT NULL,
  `deleted_date` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for assign_kepala_asrama
-- ----------------------------
DROP TABLE IF EXISTS `assign_kepala_asrama`;
CREATE TABLE `assign_kepala_asrama`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kode_asrama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT NULL,
  `created_by` bigint(20) NULL DEFAULT NULL,
  `created_date` timestamp(0) NULL DEFAULT current_timestamp(),
  `updated_by` bigint(20) NULL DEFAULT NULL,
  `updated_date` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` bigint(20) NULL DEFAULT NULL,
  `deleted_date` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for assign_mahasiswa_asrama
-- ----------------------------
DROP TABLE IF EXISTS `assign_mahasiswa_asrama`;
CREATE TABLE `assign_mahasiswa_asrama`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nim` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kode_asrama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT NULL,
  `created_by` bigint(20) NULL DEFAULT NULL,
  `created_date` timestamp(0) NULL DEFAULT current_timestamp(),
  `updated_by` bigint(20) NULL DEFAULT NULL,
  `updated_date` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` bigint(20) NULL DEFAULT NULL,
  `deleted_date` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for assign_siswa_asrama
-- ----------------------------
DROP TABLE IF EXISTS `assign_siswa_asrama`;
CREATE TABLE `assign_siswa_asrama`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nis` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kode_asrama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyblob NULL,
  `created_by` bigint(20) NULL DEFAULT NULL,
  `created_date` timestamp(0) NULL DEFAULT current_timestamp(),
  `updated_by` bigint(20) NULL DEFAULT NULL,
  `updated_date` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` bigint(20) NULL DEFAULT NULL,
  `deleted_date` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for jabatan
-- ----------------------------
DROP TABLE IF EXISTS `jabatan`;
CREATE TABLE `jabatan`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT NULL,
  `created_by` bigint(20) NULL DEFAULT NULL,
  `created_date` timestamp(0) NULL DEFAULT current_timestamp(),
  `updated_by` bigint(20) NULL DEFAULT NULL,
  `updated_date` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` bigint(20) NULL DEFAULT NULL,
  `deletd_date` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jabatan
-- ----------------------------
INSERT INTO `jabatan` VALUES (1, 'ADMIN', 1, 1, '2020-05-08 10:36:42', NULL, NULL, NULL, NULL);
INSERT INTO `jabatan` VALUES (2, 'PUKET III', 1, 1, '2020-05-08 10:37:55', NULL, NULL, NULL, NULL);
INSERT INTO `jabatan` VALUES (3, 'PKS III', 1, 1, '2020-05-08 10:38:08', NULL, NULL, NULL, NULL);
INSERT INTO `jabatan` VALUES (4, 'KEPALA ASRAMA PUTRI', 1, 1, '2020-05-08 10:38:22', NULL, NULL, NULL, NULL);
INSERT INTO `jabatan` VALUES (5, 'KEPALA ASRAMA PUTRA', 1, 1, '2020-05-08 10:38:41', NULL, NULL, NULL, NULL);
INSERT INTO `jabatan` VALUES (6, 'KEPALA ASRAMA PUTRA MAHASISWA', 1, 1, '2020-05-08 10:38:59', NULL, NULL, NULL, NULL);
INSERT INTO `jabatan` VALUES (7, 'KEPALA ASRAMA PUTRI MAHASISWA', 1, 1, '2020-05-08 10:39:15', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for kategori_asrama
-- ----------------------------
DROP TABLE IF EXISTS `kategori_asrama`;
CREATE TABLE `kategori_asrama`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode_kategori_asrama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama_kategori_asrama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT NULL,
  `created_by` bigint(20) NULL DEFAULT NULL,
  `created_date` timestamp(0) NULL DEFAULT current_timestamp(),
  `updated_by` bigint(20) NULL DEFAULT NULL,
  `updated_date` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` bigint(20) NULL DEFAULT NULL,
  `deleted_date` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kategori_asrama
-- ----------------------------
INSERT INTO `kategori_asrama` VALUES (1, 'SPUTRA', 'SISWA PUTRA', 1, 1, '2020-05-07 13:09:34', NULL, NULL, NULL, NULL);
INSERT INTO `kategori_asrama` VALUES (2, 'SPUTRI', 'SISWA PUTRI', 1, 1, '2020-05-07 13:10:28', NULL, NULL, NULL, NULL);
INSERT INTO `kategori_asrama` VALUES (3, 'MPUTRA', 'MAHASISWA PUTRA', 1, 1, '2020-05-07 13:11:07', NULL, NULL, NULL, NULL);
INSERT INTO `kategori_asrama` VALUES (4, 'MPUTRI', 'MAHASISWA PUTRI', 1, 1, '2020-05-07 13:11:29', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for mahasiswa
-- ----------------------------
DROP TABLE IF EXISTS `mahasiswa`;
CREATE TABLE `mahasiswa`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis_kelamin` enum('L','P') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tempat_lahir` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal_lahir` date NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nomor_telepon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT NULL,
  `created_by` bigint(20) NULL DEFAULT NULL,
  `created_date` timestamp(0) NULL DEFAULT current_timestamp(),
  `updated_by` bigint(20) NULL DEFAULT NULL,
  `updated_date` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` bigint(20) NULL DEFAULT NULL,
  `deleted_date` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mahasiswa
-- ----------------------------
INSERT INTO `mahasiswa` VALUES (17, '0001', 'bro', 'L', 'cimahi', '1990-01-08', 'tes', '0999', 1, 1, '2020-05-07 11:21:46', 1, '2020-05-07 06:22:50', NULL, NULL);

-- ----------------------------
-- Table structure for pegawai
-- ----------------------------
DROP TABLE IF EXISTS `pegawai`;
CREATE TABLE `pegawai`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis_kelamin` enum('L','P') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tempat_lahir` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal_lahir` date NULL DEFAULT NULL,
  `nomor_telepon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT NULL,
  `created_by` bigint(20) NULL DEFAULT NULL,
  `created_date` timestamp(0) NULL DEFAULT current_timestamp(),
  `updated_by` bigint(20) NULL DEFAULT NULL,
  `updated_date` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` bigint(20) NULL DEFAULT NULL,
  `deleted_date` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pegawai
-- ----------------------------
INSERT INTO `pegawai` VALUES (1, '00001', 'asep', 'L', 'tes', 'cimahi', '1990-01-01', '088881', 1, 1, '2020-05-07 11:36:45', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for siswa
-- ----------------------------
DROP TABLE IF EXISTS `siswa`;
CREATE TABLE `siswa`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis_kelamin` enum('L','P') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tempat_lahir` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal_lahir` date NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nomor_telepon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT NULL,
  `created_by` bigint(20) NULL DEFAULT NULL,
  `created_date` timestamp(0) NULL DEFAULT current_timestamp(),
  `updated_by` bigint(20) NULL DEFAULT NULL,
  `updated_date` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` bigint(20) NULL DEFAULT NULL,
  `deleted_date` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomor` bigint(20) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_jabatan` bigint(20) NULL DEFAULT NULL,
  `access_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT NULL,
  `created_by` bigint(20) NULL DEFAULT NULL,
  `created_date` timestamp(0) NULL DEFAULT current_timestamp(),
  `updated_by` bigint(20) NULL DEFAULT NULL,
  `updated_date` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` bigint(20) NULL DEFAULT NULL,
  `deleted_date` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 2137200781, '$2y$10$stDW3708fxUoOhvHcjGU5eOE9qD5a.Qfx0Gxcq4ocg12tEYFHIdvS', NULL, 'b1ca0f1aa805a4327f56c0fb53f4bf180655d0121c59116a0c0a7b72df68e24579389798f1b7bd811f6d801b7be038554cc2cfc27cd583ce48dcebf204fbb3e707d4f85a9a6000e7f0b544ebeeb2f16a', 1, 1, '2020-05-04 11:19:35', NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
